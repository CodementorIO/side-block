
1.1.0 / 2016-01-18
==================

[add]

 * add query string support

1.0.7 / 2016-01-14
==================

[change]

 * ratio start from .5, sideoffset to 10
 * adjust side offset to 20
 * update style: border, transitions

1.0.6 / 2016-01-14
==================

[fix]

 * fix styles:
   - apply box-sizing under all elements in plugin

1.0.5 / 2016-01-14
==================

[fix]

 * fix sytle:
   - add `box-sizing: content-box`
   - add line-height to footer

1.0.4 / 2016-01-14
==================

[change]

 * load css in `go`, to make loading from server side safe

1.0.3 / 2016-01-13
==================

[add]

 * add onshow api
 * add onclick api

[change]

 * hide when width < 1024
 * add !important to styles
 * load css on init

1.0.2 / 2016-01-13
==================

[change]

 * set parent element position, update block image height
