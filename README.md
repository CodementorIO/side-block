# SideBlock
-----------

## Dev:

run `$ npm start`


## build:
run `$ npm run build`

# Usage:

## ES6 module

```javascript
import { go } from 'sideBlock';

let el = document.querySelectorAll('.main')[0];
let categories = [ 'swift' ];
let baseUrl = 'http://localhost:8080/api'

sideBlock.go({
  element: el,
  categories: categories,
  baseUrl: baseUrl,
  onClick: function(options) {
    console.log('on click', options.ad);
  },
  onShow: function(options) {
    console.log('on show', options.ad);
  },
  queries: {
    key1: 'val1',
    key2: 'val2'
  }
});
```

## Plain JS
```html
<script src="bower_components/sideBlock/dist/bundle.js" />
```

```javascript
$(function() {
  var el = document.querySelectorAll('.main')[0];
  var categories = [ 'swift' ];
  var baseUrl = 'http://localhost:8080/api'

  sideBlock.go({
    element: el,
    categories: categories,
    baseUrl: baseUrl,
    onClick: function(options) {
      console.log('on click', options.ad);
    },
    onShow: function(options) {
      console.log('on show', options.ad);
    },
    queries: {
      key1: 'val1',
      key2: 'val2'
    }
  });
});
```
