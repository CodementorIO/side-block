import retrieveAds from './retrieveAds';
import querystring from 'querystring';

let baseClassName = "cm-side-block-ad";
let noop = function() {}

export function go({ element, categories, baseUrl, queries = {}, onClick = noop, onShow = noop }) {
  require('./style.scss');
  element.style.position = 'relative';
  function onSuccess (ads) {
    ads.forEach((ad, index)=> {
      let onShowCallback = function() {
        onShow({ ad });
      }
      let onClickCallback = function() {
        onClick({ ad });
      }
      setupEvent({
        element,
        onShow: onShowCallback,
        ratio: 0.5 + (index * 0.1),
        sideBlock: createAdElement({ ad, queries, onClick: onClickCallback }),
        side: ( index % 2 ) ? 'left' : 'right'
      });
    });
  }
  retrieveAds({ categories, baseUrl, onSuccess });
}

function setupEvent ({ element, ratio, sideBlock, side='left', onShow }) {
  let reached = false;
  let threashold = element.offsetHeight * ratio;

  sideBlock.style.top = `${threashold}px`;
  element.appendChild(sideBlock);

  function showSideBlock () {
    let sideOffset = - sideBlock.offsetWidth - 10;
    sideOffset = sideOffset + 'px';

    sideBlock.style[side] = sideOffset;
    sideBlock.className = `${sideBlock.className} ${baseClassName}--show`
    onShow();
  }

  window.addEventListener('scroll', ()=> {
    if (reached) {
      return;
    }
    let { top } = element.getBoundingClientRect();
    let bottomHeight = -top + window.innerHeight;

    if ( bottomHeight > threashold) {
      showSideBlock();
      reached = true;
    }
  })
}

function createAdElement({ ad, queries, onClick }) {
  let el;
  let qs = querystring.stringify(queries);
  qs = qs ? `?${qs}` : '';

  let str = `
  <a class="${baseClassName}" target="_blank" href="${ad.url}${qs}">
    <p class="${baseClassName}__title">${ad.title}</p>
    <div class="${baseClassName}__img-wrapper">
      <img src="${ad.image_url}" class="${baseClassName}__img"/>
    </div>
    <p class="${baseClassName}__footer">${ad.description}</p>
  </a>
  `;
  let eleHolder = document.implementation.createHTMLDocument();
  eleHolder.body.innerHTML = str;
  el = eleHolder.body.children[0];

  el.onclick = onClick;

  return el;
};
