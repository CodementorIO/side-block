import agent from 'superagent';

export default function({ categories, onSuccess, baseUrl }) {
  let query = categories.join(',');
  agent.get(`${baseUrl}/ads?q=${query}`)
    .end(function(err, res) {
      if (! err) {
        onSuccess(res.body);
      }
    })
}
